FROM maven:3.8.7-eclipse-temurin-19-alpine AS build

WORKDIR /app

COPY src src

COPY pom.xml .

RUN mvn clean package

FROM quay.io/wildfly/wildfly:latest as deploy

COPY --from=build /app/target/ProjectGift-1.0-SNAPSHOT.war /opt/jboss/wildfly/standalone/deployments/

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
