# Gift - Advanced Programing

## Access the project online

The server is hosted on my virtual private server on a Stardust Instance provided by Scaleway. You can directly acces it by the following link: https://gift.loadeksdi.com/

## Run the project locally

### First option (pulling latest image)

Just launch the `docker compose up -d` command and it will start the project by pulling the latest image from the DockerHub registry and start it with the needed database and initialization script.

## Second option (building the image locally)

Instead of starting the project by pulling it, you can build it by yourself. First you will need to run `docker build . -t loadeksdee/advanced-programming:latest` and then `docker compose up -d` so that the project start properly.
