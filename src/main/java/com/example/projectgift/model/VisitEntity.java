package com.example.projectgift.model;

import jakarta.persistence.*;

import java.sql.Date;

@Entity
@Table(name = "Visit", schema = "gift", catalog = "")
public class VisitEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "visitSheet", nullable = true)
    private Boolean visitSheet;
    @Basic
    @Column(name = "visitPlanned", nullable = true)
    private Boolean visitPlanned;
    @Basic
    @Column(name = "visitDate", nullable = true)
    private Date visitDate;
    @Basic
    @Column(name = "visitDone", nullable = true)
    private Boolean visitDone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getVisitSheet() {
        return visitSheet;
    }

    public void setVisitSheet(Boolean visitSheet) {
        this.visitSheet = visitSheet;
    }

    public Boolean getVisitPlanned() {
        return visitPlanned;
    }

    public void setVisitPlanned(Boolean visitPlanned) {
        this.visitPlanned = visitPlanned;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public Boolean getVisitDone() {
        return visitDone;
    }

    public void setVisitDone(Boolean visitDone) {
        this.visitDone = visitDone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VisitEntity that = (VisitEntity) o;

        if (id != that.id) return false;
        if (visitSheet != null ? !visitSheet.equals(that.visitSheet) : that.visitSheet != null) return false;
        if (visitPlanned != null ? !visitPlanned.equals(that.visitPlanned) : that.visitPlanned != null) return false;
        if (visitDate != null ? !visitDate.equals(that.visitDate) : that.visitDate != null) return false;
        if (visitDone != null ? !visitDone.equals(that.visitDone) : that.visitDone != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (visitSheet != null ? visitSheet.hashCode() : 0);
        result = 31 * result + (visitPlanned != null ? visitPlanned.hashCode() : 0);
        result = 31 * result + (visitDate != null ? visitDate.hashCode() : 0);
        result = 31 * result + (visitDone != null ? visitDone.hashCode() : 0);
        return result;
    }
}
