package com.example.projectgift.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Mark", schema = "gift", catalog = "")
public class MarkEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "techMark", nullable = true, precision = 0)
    private Double techMark;
    @Basic
    @Column(name = "comMark", nullable = true, precision = 0)
    private Double comMark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getTechMark() {
        return techMark;
    }

    public void setTechMark(Double techMark) {
        this.techMark = techMark;
    }

    public Double getComMark() {
        return comMark;
    }

    public void setComMark(Double comMark) {
        this.comMark = comMark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MarkEntity that = (MarkEntity) o;

        if (id != that.id) return false;
        if (techMark != null ? !techMark.equals(that.techMark) : that.techMark != null) return false;
        if (comMark != null ? !comMark.equals(that.comMark) : that.comMark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (techMark != null ? techMark.hashCode() : 0);
        result = 31 * result + (comMark != null ? comMark.hashCode() : 0);
        return result;
    }
}
