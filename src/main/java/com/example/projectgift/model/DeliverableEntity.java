package com.example.projectgift.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Deliverable", schema = "gift", catalog = "")
public class DeliverableEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "cdc", nullable = true)
    private Boolean cdc;
    @Basic
    @Column(name = "evaluationSheet", nullable = true)
    private Boolean evaluationSheet;
    @Basic
    @Column(name = "webSurvey", nullable = true)
    private Boolean webSurvey;
    @Basic
    @Column(name = "reportSent", nullable = true)
    private Boolean reportSent;
    @Basic
    @Column(name = "defense", nullable = true)
    private Boolean defense;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getCdc() {
        return cdc;
    }

    public void setCdc(Boolean cdc) {
        this.cdc = cdc;
    }

    public Boolean getEvaluationSheet() {
        return evaluationSheet;
    }

    public void setEvaluationSheet(Boolean evaluationSheet) {
        this.evaluationSheet = evaluationSheet;
    }

    public Boolean getWebSurvey() {
        return webSurvey;
    }

    public void setWebSurvey(Boolean webSurvey) {
        this.webSurvey = webSurvey;
    }

    public Boolean getReportSent() {
        return reportSent;
    }

    public void setReportSent(Boolean reportSent) {
        this.reportSent = reportSent;
    }

    public Boolean getDefense() {
        return defense;
    }

    public void setDefense(Boolean defense) {
        this.defense = defense;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeliverableEntity that = (DeliverableEntity) o;

        if (id != that.id) return false;
        if (cdc != null ? !cdc.equals(that.cdc) : that.cdc != null) return false;
        if (evaluationSheet != null ? !evaluationSheet.equals(that.evaluationSheet) : that.evaluationSheet != null)
            return false;
        if (webSurvey != null ? !webSurvey.equals(that.webSurvey) : that.webSurvey != null) return false;
        if (reportSent != null ? !reportSent.equals(that.reportSent) : that.reportSent != null) return false;
        if (defense != null ? !defense.equals(that.defense) : that.defense != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (cdc != null ? cdc.hashCode() : 0);
        result = 31 * result + (evaluationSheet != null ? evaluationSheet.hashCode() : 0);
        result = 31 * result + (webSurvey != null ? webSurvey.hashCode() : 0);
        result = 31 * result + (reportSent != null ? reportSent.hashCode() : 0);
        result = 31 * result + (defense != null ? defense.hashCode() : 0);
        return result;
    }
}
