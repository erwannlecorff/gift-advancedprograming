package com.example.projectgift.model;

import jakarta.persistence.*;

import java.sql.Date;

@Entity
@Table(name = "Internship", schema = "gift", catalog = "")
public class InternshipEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idStudent", nullable = true)
    private StudentEntity student;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idCompany", nullable = true)
    private CompanyEntity company;
    @ManyToOne
    @JoinColumn(name = "idTutor", nullable = true)
    private TutorEntity tutor;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idMark", nullable = true)
    private MarkEntity mark;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idVisit", nullable = true)
    private VisitEntity visit;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idDeliverable", nullable = true)
    private DeliverableEntity deliverable;
    @Basic
    @Column(name = "currentYear", nullable = true)
    private Integer currentYear;
    @Basic
    @Column(name = "startingDate", nullable = true)
    private Date startingDate;
    @Basic
    @Column(name = "endingDate", nullable = true)
    private Date endingDate;
    @Basic
    @Column(name = "missionDescription", nullable = true, length = 255)
    private String missionDescription;
    @Basic
    @Column(name = "commentary", nullable = true, length = 255)
    private String commentary;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StudentEntity getStudent() {
        return this.student;
    }

    public void setStudent(StudentEntity student) {
        this.student = student;
    }

    public CompanyEntity getCompany() {
        return company;
    }

    public void setCompany(CompanyEntity idCompany) {
        this.company = idCompany;
    }

    public TutorEntity getTutor() {
        return tutor;
    }

    public void setTutor(TutorEntity idTutor) {
        this.tutor = idTutor;
    }

    public MarkEntity getMark() {
        return mark;
    }

    public void setMark(MarkEntity mark) {
        this.mark = mark;
    }

    public VisitEntity getVisit() {
        return visit;
    }

    public void setVisit(VisitEntity visit) {
        this.visit = visit;
    }

    public DeliverableEntity getDeliverable() {
        return deliverable;
    }

    public void setDeliverable(DeliverableEntity deliverable) {
        this.deliverable = deliverable;
    }

    public Integer getCurrentYear() {
        return currentYear;
    }

    public void setCurrentYear(Integer currentYear) {
        this.currentYear = currentYear;
    }

    public Date getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(Date startingDate) {
        this.startingDate = startingDate;
    }

    public Date getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(Date endingDate) {
        this.endingDate = endingDate;
    }

    public String getMissionDescription() {
        return missionDescription;
    }

    public void setMissionDescription(String missionDescription) {
        this.missionDescription = missionDescription;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InternshipEntity that = (InternshipEntity) o;

        if (id != that.id) return false;
        if (student != null ? !student.equals(that.student) : that.student != null) return false;
        if (company != null ? !company.equals(that.company) : that.company != null) return false;
        if (tutor != null ? !tutor.equals(that.tutor) : that.tutor != null) return false;
        if (mark != null ? !mark.equals(that.mark) : that.mark != null) return false;
        if (visit != null ? !visit.equals(that.visit) : that.visit != null) return false;
        if (deliverable != null ? !deliverable.equals(that.deliverable) : that.deliverable != null)
            return false;
        if (currentYear != null ? !currentYear.equals(that.currentYear) : that.currentYear != null) return false;
        if (startingDate != null ? !startingDate.equals(that.startingDate) : that.startingDate != null) return false;
        if (endingDate != null ? !endingDate.equals(that.endingDate) : that.endingDate != null) return false;
        if (missionDescription != null ? !missionDescription.equals(that.missionDescription) : that.missionDescription != null)
            return false;
        if (commentary != null ? !commentary.equals(that.commentary) : that.commentary != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (student != null ? student.hashCode() : 0);
        result = 31 * result + (company != null ? company.hashCode() : 0);
        result = 31 * result + (tutor != null ? tutor.hashCode() : 0);
        result = 31 * result + (mark != null ? mark.hashCode() : 0);
        result = 31 * result + (visit != null ? visit.hashCode() : 0);
        result = 31 * result + (deliverable != null ? deliverable.hashCode() : 0);
        result = 31 * result + (currentYear != null ? currentYear.hashCode() : 0);
        result = 31 * result + (startingDate != null ? startingDate.hashCode() : 0);
        result = 31 * result + (endingDate != null ? endingDate.hashCode() : 0);
        result = 31 * result + (missionDescription != null ? missionDescription.hashCode() : 0);
        result = 31 * result + (commentary != null ? commentary.hashCode() : 0);
        return result;
    }
}
