package com.example.projectgift.dao;

import com.example.projectgift.model.CompanyEntity;
import com.example.projectgift.model.StudentEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;


public class CompanyDao {
    private final EntityManagerFactory entityManagerFactory;

    public CompanyDao(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public CompanyEntity createCompany(String name,String adress, String internshipSupervisor){

        EntityManager em = this.entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        CompanyEntity newCompany = new CompanyEntity(); // Create a new entity object
        newCompany.setName(name); // Set the entity properties
        newCompany.setAddress(adress);
        newCompany.setInternshipSupervisor(internshipSupervisor);
        em.persist(newCompany); // Persist the entity to the database
        em.getTransaction().commit();
        return  newCompany;
    }

}

