package com.example.projectgift.dao;

import com.example.projectgift.model.DeliverableEntity;
import com.example.projectgift.model.VisitEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;


public class DeliverableDao {
    private final EntityManagerFactory entityManagerFactory;

    public DeliverableDao(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public DeliverableEntity createDeliverable(boolean cdc ,boolean evaluationSheet, boolean webSurvey, boolean reportSent,boolean defense){

        EntityManager em = this.entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        DeliverableEntity  newDeliverable = new DeliverableEntity(); // Create a new entity object
        newDeliverable.setCdc(cdc);
        newDeliverable.setEvaluationSheet(evaluationSheet);
        newDeliverable.setWebSurvey(webSurvey);
        newDeliverable.setReportSent(reportSent);
        newDeliverable.setDefense(defense);
        em.persist(newDeliverable); // Persist the entity to the database
        em.getTransaction().commit();
        return  newDeliverable;
    }

}

