package com.example.projectgift.dao;

import com.example.projectgift.model.TutorEntity;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.NoResultException;
import jakarta.ws.rs.NotFoundException;
import org.mindrot.jbcrypt.BCrypt;


public class TutorDao {
    private final EntityManagerFactory entityManagerFactory;

    public TutorDao(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public TutorEntity findTutorByLogin(String login, String password) throws NotFoundException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        TutorEntity tutorEntity;
        try {
            tutorEntity = entityManager.createQuery("SELECT t FROM TutorEntity t WHERE t.username = :login", TutorEntity.class)
                    .setParameter("login", login)
                    .getSingleResult();
        } catch (NoResultException e) {
            throw new NotFoundException("Tutor not found");
        } finally {
            entityManager.close();
        }
        if(BCrypt.checkpw(password, tutorEntity.getPassword()))
            return tutorEntity;
        else
            throw new NotFoundException("Tutor not found");
    }
}

