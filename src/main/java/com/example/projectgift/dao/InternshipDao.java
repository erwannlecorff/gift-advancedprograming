package com.example.projectgift.dao;

import com.example.projectgift.model.*;
import jakarta.persistence.*;
import jakarta.ws.rs.NotFoundException;

import java.time.Year;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;


public class InternshipDao {
    private final EntityManagerFactory entityManagerFactory;
    private final StudentDao studentDao;
    private final CompanyDao companyDao;
    private final VisitDao visitDao;
    private final DeliverableDao deliverableDao;
    private final MarkDao markDao;

    public InternshipDao(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
        this.studentDao = new StudentDao(entityManagerFactory);
        this.companyDao = new CompanyDao(entityManagerFactory);
        this.visitDao = new VisitDao(entityManagerFactory);
        this.deliverableDao = new DeliverableDao(entityManagerFactory);
        this.markDao = new MarkDao(entityManagerFactory);
    }
    public void createInternship(TutorEntity tutor,String studentFirstName, String studentLastName, String studentGroup, String companyName, String companyAddress, String internshipSupervisor, Date startingDate, Date endingDate,String missionDescription,String commentary,boolean cdc ,boolean evaluationSheet, boolean webSurvey, boolean reportSent,boolean defense, boolean visitSheet, boolean visitPlanned, Date visitDate, boolean visitDone, Double techMark, Double comMark){
        StudentEntity student = this.studentDao.createStudent(studentFirstName,studentLastName,studentGroup);
        CompanyEntity company = this.companyDao.createCompany(companyName,companyAddress,internshipSupervisor);
        VisitEntity visit = this.visitDao.createVisit(visitSheet,visitPlanned,visitDate,visitDone);
        DeliverableEntity deliverable = this.deliverableDao.createDeliverable(cdc ,evaluationSheet,webSurvey,reportSent,defense);
        MarkEntity mark = this.markDao.createMark(techMark,comMark);

        EntityManager em = this.entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        InternshipEntity newIntership = new InternshipEntity(); // Create a new entity object
        newIntership.setStudent(student);
        newIntership.setCompany(company);
        newIntership.setTutor(tutor);
        newIntership.setMark(mark);
        newIntership.setVisit(visit);
        newIntership.setDeliverable(deliverable);
        newIntership.setCurrentYear(Year.now().getValue());
        newIntership.setStartingDate(startingDate);
        newIntership.setEndingDate(endingDate);
        newIntership.setMissionDescription(missionDescription);
        newIntership.setCommentary(commentary);

        em.persist(newIntership); // Persist the entity to the database
        em.getTransaction().commit();
    }
    public List<Object[]> findInternshipByTutor(String username) throws NotFoundException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Object[]> results = new ArrayList<>();
        try {
            TypedQuery<Object[]> query = entityManager.createQuery("" +
                    "SELECT i.id ,s.classGroup,s.firstname,s.lastname, d.cdc,v.visitSheet,d.evaluationSheet,d.webSurvey," +
                    "d.reportSent,d.defense,v.visitPlanned,v.visitDate,v.visitDone," +
                    "i.startingDate,i.endingDate,c.name,c.internshipSupervisor,c.address,COALESCE(m.techMark,'na') ,COALESCE(m.comMark,'na')  " +
                    "FROM InternshipEntity i " +
                    "JOIN i.tutor t " +
                    "JOIN i.company c " +
                    "JOIN i.deliverable d " +
                    "JOIN i.mark m " +
                    "JOIN i.student s " +
                    "JOIN i.visit v "+
                    "WHERE t.username = :username"+
                    "", Object[].class);
            query.setParameter("username", username);
            results = query.getResultList();
        } catch (NoResultException e) {
            throw new NotFoundException("Tutor not found");
        } finally {
            entityManager.close();
        }
        return results;
    }

    public int removeInternshipById(int id) {
        EntityManager em = entityManagerFactory.createEntityManager();
        int deletedCount = 0;
        try {
            em.getTransaction().begin();
            Query query = em.createQuery("DELETE FROM InternshipEntity e WHERE e.id = :id");
            query.setParameter("id", id);
            deletedCount = query.executeUpdate();
            em.getTransaction().commit();
        } catch (NoResultException e) {
            throw new NotFoundException("Tutor not found");
        }
        return deletedCount;
    }

    public List<Object[]> getInternshipDetails(int id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Object[]> results = new ArrayList<>();
        try {
            TypedQuery<Object[]> query = entityManager.createQuery("" +
                    "SELECT i.id,s.lastname,s.firstname,s.classGroup, c.name,c.address,c.internshipSupervisor,i.startingDate," +
                    "i.endingDate,i.missionDescription,i.commentary, d.cdc, d.evaluationSheet,d.webSurvey,d.reportSent,d.defense, " +
                    "v.visitSheet,v.visitPlanned,v.visitDone,v.visitDate, m.techMark,m.comMark "+
                    "FROM InternshipEntity i " +
                    "JOIN i.company c " +
                    "JOIN i.student s " +
                    "JOIN i.deliverable d " +
                    "JOIN i.visit v " +
                    "JOIN i.mark m " +
                    "WHERE i.id = :id"+
                    "", Object[].class);
            query.setParameter("id", id);
            results = query.getResultList();
        } catch (NoResultException e) {
            throw new NotFoundException("Internship not found");
        } finally {
            entityManager.close();
        }
        return results;
    }
    public void updateInternship(TutorEntity tutor,int idInternship,String studentFirstName, String studentLastName, String studentGroup, String companyName, String companyAddress, String internshipSupervisor, Date startingDate, Date endingDate,String missionDescription,String commentary,boolean cdc ,boolean evaluationSheet, boolean webSurvey, boolean reportSent,boolean defense, boolean visitSheet, boolean visitPlanned, Date visitDate, boolean visitDone, Double techMark, Double comMark){

        EntityManager em = this.entityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        InternshipEntity internship = em.find(InternshipEntity.class,idInternship);
        internship.setCommentary(commentary);
        internship.setMissionDescription(missionDescription);
        internship.setEndingDate(endingDate);
        internship.setStartingDate(startingDate);

        StudentEntity student = em.find(StudentEntity.class,internship.getStudent().getId());
        student.setFirstname(studentFirstName);
        student.setLastname(studentLastName);
        student.setClassGroup(studentGroup);

        CompanyEntity company = em.find(CompanyEntity.class,internship.getCompany().getId());
        company.setInternshipSupervisor(internshipSupervisor);
        company.setAddress(companyAddress);
        company.setName(companyName);

        VisitEntity visit = em.find(VisitEntity.class,internship.getVisit().getId());
        visit.setVisitDone(visitDone);
        visit.setVisitPlanned(visitPlanned);
        visit.setVisitDate(visitDate);
        visit.setVisitSheet(visitSheet);

        DeliverableEntity deliverable = em.find(DeliverableEntity.class,internship.getDeliverable().getId());
        deliverable.setDefense(defense);
        deliverable.setReportSent(reportSent);
        deliverable.setWebSurvey(webSurvey);
        deliverable.setCdc(cdc);
        deliverable.setEvaluationSheet(evaluationSheet);

        MarkEntity mark = em.find(MarkEntity.class,internship.getMark().getId());
        mark.setTechMark(techMark);
        mark.setComMark(comMark);

        em.getTransaction().commit();
        em.close();
    }
}

