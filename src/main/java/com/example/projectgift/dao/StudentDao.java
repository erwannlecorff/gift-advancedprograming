package com.example.projectgift.dao;

import com.example.projectgift.model.StudentEntity;
import jakarta.persistence.*;
import jakarta.ws.rs.NotFoundException;

import java.util.ArrayList;
import java.util.List;


public class StudentDao {
    private final EntityManagerFactory entityManagerFactory;

    public StudentDao(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public StudentEntity createStudent(String firstname,String lastname, String group){
        //createStudent
        EntityManager em = this.entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        StudentEntity newStudent = new StudentEntity(); // Create a new entity object
        newStudent.setFirstname(firstname); // Set the entity properties
        newStudent.setLastname(lastname);
        newStudent.setClassGroup(group);
        em.persist(newStudent); // Persist the entity to the database
        em.getTransaction().commit();
        return  newStudent;
    }
}

