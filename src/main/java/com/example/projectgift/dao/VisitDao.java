package com.example.projectgift.dao;

import com.example.projectgift.model.CompanyEntity;
import com.example.projectgift.model.VisitEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

import java.sql.Date;


public class VisitDao {
    private final EntityManagerFactory entityManagerFactory;

    public VisitDao(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public VisitEntity createVisit(boolean visitSheet, boolean visitPlanned, Date visitDate,boolean visitDone){

        EntityManager em = this.entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        VisitEntity newVisit = new VisitEntity(); // Create a new entity object
        newVisit.setVisitSheet(visitSheet);
        newVisit.setVisitPlanned(visitPlanned);
        newVisit.setVisitDate(visitDate);
        newVisit.setVisitDone(visitDone);
        em.persist(newVisit); // Persist the entity to the database
        em.getTransaction().commit();
        return  newVisit;
    }

}

