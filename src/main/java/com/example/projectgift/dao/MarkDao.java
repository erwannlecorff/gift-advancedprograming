package com.example.projectgift.dao;

import com.example.projectgift.model.MarkEntity;
import com.example.projectgift.model.StudentEntity;
import com.example.projectgift.model.VisitEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;


public class MarkDao {
    private final EntityManagerFactory entityManagerFactory;

    public MarkDao(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public MarkEntity createMark(Double techMark, Double comMark){

        EntityManager em = this.entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        MarkEntity newMark = new MarkEntity(); // Create a new entity object
        newMark.setTechMark(techMark);
        newMark.setComMark(comMark);
        em.persist(newMark); // Persist the entity to the database
        em.getTransaction().commit();
        return newMark;
    }

}

