package com.example.projectgift.controller;

import com.example.projectgift.dao.InternshipDao;
import com.example.projectgift.dao.TutorDao;
import com.example.projectgift.helper.SessionLogin;
import com.example.projectgift.model.TutorEntity;
import jakarta.persistence.Persistence;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.ws.rs.NotFoundException;

import java.io.Console;
import java.io.IOException;
import java.util.List;

@WebServlet(name="TutorInterface", value = "/TutorInterface")
public class TutorInterfaceServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        //fait la requète pour renvoyer toute les données nécessaire pour les details
        if(!SessionLogin.isConnected(req)){
            req.setAttribute("error", "You are not connected");
            req.getRequestDispatcher("WEB-INF/index.jsp").forward(req, resp);
            return;
        }
        InternshipDao internshipDao = new InternshipDao(Persistence.createEntityManagerFactory("gift"));

        try {
            List<Object[]> result = internshipDao.findInternshipByTutor(req.getSession().getAttribute("username").toString());
            req.setAttribute("results",result);
            req.getRequestDispatcher("WEB-INF/welcome.jsp").forward(req, resp);
        } catch (NotFoundException e) {
            req.getRequestDispatcher("WEB-INF/index.jsp").forward(req, resp);
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(!SessionLogin.isConnected(req)){
            req.setAttribute("error", "You are not connected");
            req.getRequestDispatcher("WEB-INF/index.jsp").forward(req, resp);
            return;
        }
        String action = req.getParameter("action");
        if ("remove".equals(action)) {
            removeRoute(req, resp);
        }
        else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private void removeRoute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InternshipDao internshipDao = new InternshipDao(Persistence.createEntityManagerFactory("gift"));

        try {
            int deleted = internshipDao.removeInternshipById(Integer.parseInt(req.getParameter("id")));
        } catch (NotFoundException e) {
            req.setAttribute("error", "Row cannot be removed");
            req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
        }

        //redirection pour actualisation
        List<Object[]> result = internshipDao.findInternshipByTutor(req.getSession().getAttribute("username").toString());
        req.setAttribute("results",result);
        req.getRequestDispatcher("WEB-INF/welcome.jsp").forward(req, resp);

    }

}

