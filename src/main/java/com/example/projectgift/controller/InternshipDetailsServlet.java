package com.example.projectgift.controller;

import com.example.projectgift.dao.InternshipDao;
import com.example.projectgift.helper.SessionLogin;
import com.example.projectgift.model.TutorEntity;
import jakarta.persistence.Persistence;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.NotFoundException;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

@WebServlet(name="InternshipDetails", value = "/InternshipDetails")
public class InternshipDetailsServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        if(!SessionLogin.isConnected(req)){
            req.setAttribute("error", "You are not connected");
            req.getRequestDispatcher("WEB-INF/index.jsp").forward(req, resp);
            return;
        }
        String action = req.getParameter("action");
        if ("add".equals(action)) {
            showAdd(req,resp);
        } else if ("update".equals(action)) {
            showUpdate(req, resp);
        } else {
            // Invalid request
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(!SessionLogin.isConnected(req)){
            req.setAttribute("error", "You are not connected");
            req.getRequestDispatcher("WEB-INF/index.jsp").forward(req, resp);
            return;
        }
        String action = req.getParameter("action");
        if ("add".equals(action)) {
            addRoute(req, resp);
        } else if ("update".equals(action)) {
            updateRoute(req, resp);
        } else {
            // Invalid request
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
    private Double stringToInt(String s){
        Double ret;
        if(s==""||s==null){
            ret = null;
        }
        else {
            try
            {
                ret = Double.parseDouble(s);
            }
            catch(NumberFormatException e)
            {
                ret = null;
            }
        }
        return ret;
    }
    private  Date stringToDate(String s){
        Date ret = null;
        if(!(s=="")&&!(s==null)){
            ret = Date.valueOf(s);
        }
        return ret;
    }
    private void addRoute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InternshipDao internshipDao = new InternshipDao(Persistence.createEntityManagerFactory("gift"));
        Double techMark,comMark;
        Date visitDate;
        techMark = stringToInt(req.getParameter("techMark"));
        comMark = stringToInt(req.getParameter("comMark"));
        visitDate = stringToDate(req.getParameter("visitDate"));
        try {
            internshipDao.createInternship((TutorEntity) req.getSession().getAttribute("tutor"),req.getParameter("studentFirstname"),req.getParameter("studentLastname"),
                    req.getParameter("studentGroup"),req.getParameter("companyName"),req.getParameter("companyAddress"),req.getParameter("internshipSupervisor"),
                    Date.valueOf(req.getParameter("startingDate")),Date.valueOf(req.getParameter("endingDate")),req.getParameter("missionDescription"),
                    req.getParameter("commentary"),"on".equals(req.getParameter("cdc")),"on".equals(req.getParameter("evaluationSheet")),
                    "on".equals(req.getParameter("webSurvey")),"on".equals(req.getParameter("reportSent")),
                    "on".equals(req.getParameter("defense")),"on".equals(req.getParameter("visitSheet")),
                    "on".equals(req.getParameter("visitPlanned")), visitDate,
                    "on".equals(req.getParameter("visitDone")),techMark,comMark);
        } catch (NotFoundException e) {
            req.setAttribute("error", "Row cannot be added");
            req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
        }
        List<Object[]> result = internshipDao.findInternshipByTutor(req.getSession().getAttribute("username").toString());
        req.setAttribute("results",result);
        req.getRequestDispatcher("/WEB-INF/welcome.jsp").forward(req, resp);
    }

    private void updateRoute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InternshipDao internshipDao = new InternshipDao(Persistence.createEntityManagerFactory("gift"));
        Double techMark,comMark;
        Date visitDate;
        techMark = stringToInt(req.getParameter("techMark"));
        comMark = stringToInt(req.getParameter("comMark"));
        visitDate = stringToDate(req.getParameter("visitDate"));
        try {
            internshipDao.updateInternship((TutorEntity) req.getSession().getAttribute("tutor"),Integer.parseInt(req.getParameter("idInternship")),
                    req.getParameter("studentFirstname"),req.getParameter("studentLastname"),req.getParameter("studentGroup"),
                    req.getParameter("companyName"),req.getParameter("companyAddress"),req.getParameter("internshipSupervisor"),
                    Date.valueOf(req.getParameter("startingDate")),Date.valueOf(req.getParameter("endingDate")),req.getParameter("missionDescription"),
                    req.getParameter("commentary"),"on".equals(req.getParameter("cdc")),"on".equals(req.getParameter("evaluationSheet")),
                    "on".equals(req.getParameter("webSurvey")),"on".equals(req.getParameter("reportSent")),
                    "on".equals(req.getParameter("defense")),"on".equals(req.getParameter("visitSheet")),
                    "on".equals(req.getParameter("visitPlanned")),visitDate ,
                    "on".equals(req.getParameter("visitDone")),techMark,comMark);
        } catch (NotFoundException e) {
            req.setAttribute("error", "Row cannot be updated");
            req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
        }
        List<Object[]> result = internshipDao.findInternshipByTutor(req.getSession().getAttribute("username").toString());
        req.setAttribute("results",result);
        req.getRequestDispatcher("/WEB-INF/welcome.jsp").forward(req, resp);
    }
    private void showUpdate(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("action",req.getParameter("update"));
        InternshipDao internshipDao = new InternshipDao(Persistence.createEntityManagerFactory("gift"));
        try {
            List<Object[]> ret = internshipDao.getInternshipDetails(Integer.parseInt(req.getParameter("id")));
            req.setAttribute("results",ret);
        } catch (NotFoundException e) {
            req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
        }
        req.getRequestDispatcher("/WEB-INF/details.jsp").forward(req, resp);
    }

    private void showAdd(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/details.jsp").forward(req, response);
    }

}

