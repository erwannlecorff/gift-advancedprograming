package com.example.projectgift.controller;

import com.example.projectgift.dao.InternshipDao;
import com.example.projectgift.dao.TutorDao;
import com.example.projectgift.model.TutorEntity;
import jakarta.persistence.Persistence;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.ws.rs.NotFoundException;
import org.mindrot.jbcrypt.BCrypt;

import java.io.IOException;
import java.util.List;

@WebServlet(name="Login", value = "/")
public class LoginServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        if ("logout".equals(action)) {
            req.getSession().invalidate();
        }
        req.getRequestDispatcher("WEB-INF/index.jsp").forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        TutorDao tutorDao = new TutorDao(Persistence.createEntityManagerFactory("gift"));
        InternshipDao internshipDao = new InternshipDao(Persistence.createEntityManagerFactory("gift"));

        try {
            TutorEntity tutor = tutorDao.findTutorByLogin(login, password);
            HttpSession session = req.getSession();
            session.setAttribute("lastname", tutor.getLastname());
            session.setAttribute("firstname", tutor.getFirstname());
            session.setAttribute("username", tutor.getUsername());
            session.setAttribute("tutor",tutor);
            List<Object[]> result = internshipDao.findInternshipByTutor(session.getAttribute("username").toString());
            req.setAttribute("results",result);

            req.getRequestDispatcher("WEB-INF/welcome.jsp").forward(req, resp);
        } catch (NotFoundException e) {
            req.setAttribute("error", "Invalid login or password");
            req.getRequestDispatcher("WEB-INF/index.jsp").forward(req, resp);
        }
    }
}

