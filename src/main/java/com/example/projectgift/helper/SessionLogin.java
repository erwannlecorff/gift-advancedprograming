package com.example.projectgift.helper;

import jakarta.servlet.http.HttpServletRequest;

public class SessionLogin {

    public static boolean isConnected(HttpServletRequest req){
        return req.getSession().getAttribute("username")!= null && req.getSession().getAttribute("username")!="";
    }
}
