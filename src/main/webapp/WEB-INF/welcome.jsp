<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title>GIFT - Welcome</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
    <script src="https://cdn.tailwindcss.com"></script>
    <style>
        /* ===== Scrollbar CSS ===== */
        /* Firefox */
        * {
            scrollbar-width: auto;
            scrollbar-color: #0c4a6e #ffffff;
        }

        /* Chrome, Edge, and Safari */
        *::-webkit-scrollbar {
            height: 10px;
        }

        *::-webkit-scrollbar-track {
            background: #ffffff;
        }

        *::-webkit-scrollbar-thumb {
            background-color: #0c4a6e;
            border-radius: 0px;
            border: 0px solid #ffffff;
        }
    </style>
</head>
<body>
    <header class="bg-sky-900">
        <nav class="mx-auto flex max-w-7xl items-center justify-between p-6 px-8">
            <div class="flex flex-1">
                <img class="h-8 w-auto" src="<%=request.getContextPath()%>/img/logo-blanc-small.png" alt="logo efrei">
                <span class="text-xl text-white tracking-wide ml-3 font-semibold">Great Intern Follow-up Tool</span>
            </div>
            <div class="flex flex-1 justify-end">
                <svg class="h-6 w-6 text-white"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <circle cx="12" cy="7" r="4" />  <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" /></svg>
                <span class="text-l text-white tracking-wide ml-3 font-medium">${ sessionScope.firstname } ${ sessionScope.lastname }</span>
                <svg class="h-6 w-6 text-white ml-3"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2" />  <path d="M7 12h14l-3 -3m0 6l3 -3" /></svg>
                <a href="#" onclick="(logout())"class="text-l text-white tracking-wide ml-3 font-medium">
                    Log Out
                </a>
            </div>
        </nav>
    </header>
    <section class="mx-auto max-w-7xl p-6 px-8 mt-4">
        <div class="flex justify-between items-end">
            <div>
                <h1 class="text-3xl text-sky-900 tracking-wide ml-3 font-bold">
                    Welcome to your teacher dashboard, ${ sessionScope.firstname } ${ sessionScope.lastname }.
                </h1>
                <p class="text-sm text-black tracking-wide ml-4 mt-2 font-normal">
                    Using this application, you will be able to manage the interships of the students for whom you are the tutor.
                </p>
            </div>
            <div>
                <a class="bg-sky-900 hover:bg-orange-400 text-white font-bold py-2 px-4 rounded" href="#" onclick="(addIntership())">
                    Add intership
                </a>
            </div>
        </div>
    </section>
    <section class="mx-auto max-w-7xl p-6 px-8">
        <div class="relative overflow-x-auto shadow-md rounded-lg">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 whitespace-nowrap">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Actions
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Group
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Student
                    </th>
                    <th scope="col" class="px-6 py-3">
                        CDC
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Visit sheet
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Entreprise sheet
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Web survey
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Submitted report
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Defense
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Visit planified
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Visit date
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Visit done
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Start date
                    </th>
                    <th scope="col" class="px-6 py-3">
                        End date
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Company
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Internship supervisor
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Address
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Tech grade
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Com grade
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="result" items="${results}">

                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <td class="flex px-6 py-4">
                        <a href="#" onclick="updateInternship(${result[0]})">
                            <svg class="h-6 w-6 text-sky-900"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M9 5H7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2V7a2 2 0 0 0 -2 -2h-2" />  <rect x="9" y="3" width="6" height="4" rx="2" />
                                <line x1="9" y1="12" x2="9.01" y2="12" />
                                <line x1="13" y1="12" x2="15" y2="12" />
                                <line x1="9" y1="16" x2="9.01" y2="16" />
                                <line x1="13" y1="16" x2="15" y2="16" />
                            </svg>
                        </a>
                        <a href="#" onclick="removeInternship(${result[0]})">
                            <svg class="h-6 w-6 text-red-700"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                            </svg>
                        </a>
                    </td>
                    <td class="px-6 py-4">
                            ${result[1]}
                    </td>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            ${result[2]} ${result[3]}
                    </th>
                    <td class="px-6 py-4">
                            ${result[4]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[5]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[6]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[7]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[8]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[9]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[10]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[11]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[12]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[13]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[14]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[15]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[16]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[17]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[18]}
                    </td>
                    <td class="px-6 py-4">
                            ${result[19]}
                    </td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </section>
</body>
<script>
    function removeInternship(id) {
        var form = document.createElement("form");
        form.method = "POST";
        form.action = "TutorInterface";

        var idInput = document.createElement("input");
        idInput.type = "hidden";
        idInput.name = "id";
        idInput.value = id;

        var action = document.createElement("input");
        action.type = "hidden";
        action.name = "action";
        action.value = "remove";

        form.appendChild(idInput);
        form.appendChild(action);
        document.body.appendChild(form);
        form.submit();
    }
    function updateInternship(id){
        var form = document.createElement("form");
        form.method = "GET";
        form.action = "InternshipDetails";

        var idInput = document.createElement("input");
        idInput.type = "hidden";
        idInput.name = "id";
        idInput.value = id;

        var action = document.createElement("input");
        action.type = "hidden";
        action.name = "action";
        action.value = "update";

        form.appendChild(idInput);
        form.appendChild(action);
        document.body.appendChild(form);
        form.submit();
    }
    function addIntership(){
        var form = document.createElement("form");
        form.method = "GET";
        form.action = "InternshipDetails";

        var action = document.createElement("input");
        action.type = "hidden";
        action.name = "action";
        action.value = "add";
        form.appendChild(action);
        document.body.appendChild(form);
        form.submit();
    }
    function logout(){
        var form = document.createElement("form");
        form.method = "GET";
        form.action = "Login";

        var action = document.createElement("input");
        action.type = "hidden";
        action.name = "action";
        action.value = "logout";
        form.appendChild(action);
        document.body.appendChild(form);
        form.submit();
    }
</script>
</html>
