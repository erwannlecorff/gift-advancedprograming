<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <html>

    <head>
        <title>GIFT - Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" sizes="32x32" href="<%=request.getContextPath()%>/img/favicon-32x32.png">
        <script src="https://cdn.tailwindcss.com"></script>
    </head>

    <body>
        <div class="lg:flex">
            <div class="hidden lg:flex items-center justify-center bg-indigo-100 flex-1 h-screen">
                <div class="max-w-sm transform duration-500 hover:scale-125">
                    <img src="<%=request.getContextPath()%>/img/logo-big.png" alt="Logo EFREI Panthéon" />
                </div>
            </div>
            <div class="lg:w-1/2 xl:max-w-screen-sm">
                <div class="py-12 bg-indigo-100 lg:bg-white flex justify-center lg:justify-start lg:px-12">
                    <div class="flex items-center">
                        <div>
                            <img class="w-10" src="<%=request.getContextPath()%>/img/logo-small.png" alt="Logo EFREI" />
                        </div>
                        <div class="text-2xl text-sky-900 tracking-wide ml-3 font-semibold">Great Intern Follow-up Tool
                        </div>
                    </div>
                </div>
                <div class="mt-10 px-12 sm:px-24 md:px-48 lg:px-12 lg:mt-16 xl:px-24 xl:max-w-2xl">
                    <h2 class="text-center text-4xl text-sky-900 font-display font-semibold lg:text-left xl:text-5xl
                        xl:text-bold">Log in</h2>
                    <div class="mt-12">
                        <% if (request.getParameter("error") !=null) { %>
                            <p style="color: red;">Invalid email or password</p>
                            <% } %>
                                <form method="post" action="Login">
                                    <div>
                                        <div class="text-sm font-bold text-gray-700 tracking-wide">Login</div>
                                        <input
                                            class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                                            name="login" type="text" placeholder="john.doe" required>
                                    </div>
                                    <div class="mt-8">
                                        <div class="flex justify-between items-center">
                                            <div class="text-sm font-bold text-gray-700 tracking-wide">
                                                Password
                                            </div>
                                        </div>
                                        <input
                                            class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                                            name="password" type="password" placeholder="Enter your password" required>
                                    </div>
                                    <p>${requestScope.get("error")}</p>
                                    <div class="mt-10">
                                        <button type="submit" class="bg-sky-900 text-gray-100 p-4 w-full rounded-full tracking-wide
                                    font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-orange-400 hover:duration-500
                                    shadow-lg">
                                            Log In
                                        </button>
                                    </div>
                                </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>