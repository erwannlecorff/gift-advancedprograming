<!DOCTYPE html>
<html>
<head>
    <title>GIFT - Add</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
<header class="bg-sky-900">
    <nav class="mx-auto flex max-w-7xl items-center justify-between p-6 px-8">
        <div class="flex flex-1">
            <img class="h-8 w-auto" src="img/logo-blanc-small.png" alt="logo efrei">
            <span class="text-xl text-white tracking-wide ml-3 font-semibold">Great Intern Follow-up Tool</span>
        </div>
        <div class="flex flex-1 justify-end">
            <svg class="h-6 w-6 text-white"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <circle cx="12" cy="7" r="4" />
                <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
            </svg>
            <span class="text-l text-white tracking-wide ml-3 font-medium">${ sessionScope.firstname } ${ sessionScope.lastname }</span>
            <svg class="h-6 w-6 text-white ml-3"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2" />  <path d="M7 12h14l-3 -3m0 6l3 -3" /></svg>
            <a href="#" onclick="(logout())" class="text-l text-white tracking-wide ml-3 font-medium">
                Log Out
            </a>
        </div>
    </nav>
</header>
<section class="mx-auto max-w-7xl p-6 px-8 mt-4">
    <div class="flex justify-between items-end">
        <div>
            <h1 class="text-3xl text-sky-900 tracking-wide font-bold">
                Welcome to the adding form, ${ sessionScope.firstname } ${ sessionScope.lastname }.
            </h1>
            <p class="text-sm text-black tracking-wide mt-2 font-normal">
                Using this form, you will be able to manage an intership.
            </p>
        </div>
    </div>
</section>

<section class="mx-auto max-w-7xl p-6 px-8">
    <form method="POST" action="InternshipDetails">
        <input type="hidden" id="idInternship" name="idInternship" value="${results[0][0]}">
        <h2 class="text-base mt-3 mb-6 font-bold uppercase text-sky-900">
            Student informations
        </h2>
        <div class="grid grid-cols-3 gap-16">
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Group</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="text" id="studentGroup" name="studentGroup" value="${results[0][3]}" required>
            </div>
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Lastname</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="text" id="studentLastname" name="studentLastname" value="${results[0][1]}" required>
            </div>
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Firstname</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="text" id="studentFirstname" name="studentFirstname" value="${results[0][2]}" required>
            </div>
        </div>
        <h2 class="text-base mb-6 mt-10 font-bold uppercase text-sky-900">
            Internship informations
        </h2>
        <div class="grid grid-cols-2 gap-16">
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Company</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="text" id="companyName" name="companyName" value="${results[0][4]}" required>
            </div>
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Internship supervisor</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="text" id="internshipSupervisor" name="internshipSupervisor" value="${results[0][6]}" required>
            </div>
        </div>
        <div class="mt-8">
            <div class="text-sm font-bold text-gray-700 tracking-wide">Address</div>
            <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="text" id="companyAddress" name="companyAddress" value="${results[0][5]}" required>
        </div>
        <div class="grid grid-cols-2 gap-16 mt-8">
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Start date</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="date" id="startingDate" name="startingDate" value="${results[0][7]}" required>
            </div>
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">End date</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="date" id="endingDate" name="endingDate" value="${results[0][8]}" required>
            </div>
        </div>
        <div class="mt-8">
            <div class="text-sm font-bold text-gray-700 tracking-wide">Description</div>
            <textarea class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" id="missionDescription" name="missionDescription" >${results[0][9]}</textarea>
        </div>
        <div class="mt-8">
            <div class="text-sm font-bold text-gray-700 tracking-wide">Comments</div>
            <textarea class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" id="commentary" name="commentary" >${results[0][10]}</textarea>
        </div>
        <h2 class="text-base mb-6 mt-10 font-bold uppercase text-sky-900">
            Deliverable informations
        </h2>
        <div class="grid grid-cols-5 gap-16 mt-8">
            <label class="relative inline-flex items-center cursor-pointer">
                <input type="checkbox" name="cdc" id="cdc" ${results[0][11] == 'true' ? 'checked' : ''} class="sr-only peer">
                <div class="w-11 h-6 bg-gray-200 rounded-full peer peer-checked:after:translate-x-full after:content-['']
                        after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border
                        after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-sky-900">
                </div>
                <span class="ml-3 text-sm font-medium text-gray-700">CDC</span>
            </label>
            <label class="relative inline-flex items-center cursor-pointer">
                <input type="checkbox" name="evaluationSheet" id="evaluationSheet" ${results[0][12] == 'true' ? 'checked' : ''} class="sr-only peer">
                <div class="w-11 h-6 bg-gray-200 rounded-full peer peer-checked:after:translate-x-full after:content-['']
                        after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border
                        after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-sky-900">
                </div>
                <span class="ml-3 text-sm font-medium text-gray-700">Enterprise sheet</span>
            </label>
            <label class="relative inline-flex items-center cursor-pointer">
                <input type="checkbox" name="webSurvey" id="webSurvey" ${results[0][13] == 'true' ? 'checked' : ''} class="sr-only peer">
                <div class="w-11 h-6 bg-gray-200 rounded-full peer peer-checked:after:translate-x-full after:content-['']
                        after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border
                        after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-sky-900">
                </div>
                <span class="ml-3 text-sm font-medium text-gray-700">Web survey</span>
            </label>
            <label class="relative inline-flex items-center cursor-pointer">
                <input type="checkbox" name="reportSent" id="reportSent" ${results[0][14] == 'true' ? 'checked' : ''} class="sr-only peer">
                <div class="w-11 h-6 bg-gray-200 rounded-full peer peer-checked:after:translate-x-full after:content-['']
                        after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border
                        after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-sky-900">
                </div>
                <span class="ml-3 text-sm font-medium text-gray-700">Submitted report</span>
            </label>
            <label class="relative inline-flex items-center cursor-pointer">
                <input type="checkbox" name="defense" id="defense" ${results[0][15] == 'true' ? 'checked' : ''}  class="sr-only peer">
                <div class="w-11 h-6 bg-gray-200 rounded-full peer peer-checked:after:translate-x-full after:content-['']
                        after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border
                        after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-sky-900">
                </div>
                <span class="ml-3 text-sm font-medium text-gray-700">Defense</span>
            </label>
        </div>
        <h2 class="text-base mb-6 mt-10 font-bold uppercase text-sky-900">
            Visit informations
        </h2>
        <div class="grid grid-cols-3 gap-16 mt-8">
            <label class="relative inline-flex items-center cursor-pointer">
                <input type="checkbox" name="visitSheet" id="visitSheet" ${results[0][16] == 'true' ? 'checked' : ''}  class="sr-only peer">
                <div class="w-11 h-6 bg-gray-200 rounded-full peer peer-checked:after:translate-x-full after:content-['']
                        after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border
                        after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-sky-900">
                </div>
                <span class="ml-3 text-sm font-medium text-gray-700">Visit sheet</span>
            </label>
            <label class="relative inline-flex items-center cursor-pointer">
                <input type="checkbox" name="visitPlanned" id="visitPlanned" ${results[0][17] == 'true' ? 'checked' : ''} class="sr-only peer">
                <div class="w-11 h-6 bg-gray-200 rounded-full peer peer-checked:after:translate-x-full after:content-['']
                        after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border
                        after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-sky-900">
                </div>
                <span class="ml-3 text-sm font-medium text-gray-700">Visit planified</span>
            </label>
            <label class="relative inline-flex items-center cursor-pointer">
                <input type="checkbox" name="visitDone" id="visitDone" ${results[0][18] == 'true' ? 'checked' : ''} class="sr-only peer">
                <div class="w-11 h-6 bg-gray-200 rounded-full peer peer-checked:after:translate-x-full after:content-['']
                        after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border
                        after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-sky-900">
                </div>
                <span class="ml-3 text-sm font-medium text-gray-700">Visit done</span>
            </label>
        </div>
        <div class="grid gap-16 mt-8">
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Visit date</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="date" id="visitDate" name="visitDate" value="${results[0][19]}">
            </div>
        </div>
        <h2 class="text-base mb-6 mt-10 font-bold uppercase text-sky-900">
            Grade informations
        </h2>
        <div class="grid grid-cols-2 gap-16">
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Tech grade</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="number" min="0" max="20" id="techMark" name="techMark" value="${results[0][20]}">
            </div>
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Com grade</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="number" min="0" max="20" id="comMark" name="comMark" value="${results[0][21]}">
            </div>
        </div>
        <%
            String parameterValue = request.getParameter("action");
            if (parameterValue != null && parameterValue.equals("update")) {
        %>
        <input type="hidden" id="action" name="action" value="update">
        <br>
        <%
            }
        %>
        <%
            if (parameterValue != null && parameterValue.equals("add")) {
        %>
        <input type="hidden" id="action" name="action" value="add">
        <br>
        <%
            }
        %>
        <div class="mt-10">
            <div class="grid grid-cols-2 gap-16">
                <button type="submit" class="bg-sky-900 text-gray-100 p-4 w-full rounded-full tracking-wide
                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-orange-400 hover:duration-500
                        shadow-lg">
                    Submit
                </button>
                <a href="#" onclick="getInternship()" class="bg-gray-700 text-gray-100 p-4 w-full rounded-full tracking-wide
                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-orange-400 hover:duration-500
                        shadow-lg text-center">
                    Go back to dashboard
                </a>
            </div>
        </div>
    </form>
</section>
<script>
    function getInternship() {
        var form = document.createElement("form");
        form.method = "GET";
        form.action = "TutorInterface";
        document.body.appendChild(form);
        form.submit();
    }
    function logout(){
        var form = document.createElement("form");
        form.method = "GET";
        form.action = "Login";

        var action = document.createElement("input");
        action.type = "hidden";
        action.name = "action";
        action.value = "logout";
        form.appendChild(action);
        document.body.appendChild(form);
        form.submit();
    }
</script>
</body>
</html>