USE gift;

CREATE TABLE Tutor (
                       id int PRIMARY KEY AUTO_INCREMENT,
                       username varchar(255),
                       password varchar(255),
                       firstname varchar(255),
                       lastname varchar(255),
                       UNIQUE (username)
);

CREATE TABLE Student (
                         id int PRIMARY KEY AUTO_INCREMENT,
                         firstname varchar(255),
                         lastname varchar(255),
                         classGroup varchar(255)
);

CREATE TABLE Internship (
                            id int PRIMARY KEY AUTO_INCREMENT,
                            idStudent int,
                            idCompany int,
                            idTutor int,
                            idMark int,
                            idVisit int,
                            idDeliverable int,
                            currentYear int,
                            startingDate date,
                            endingDate date,
                            missionDescription varchar(255),
                            commentary varchar(255)
);

CREATE TABLE Company (
                         id int PRIMARY KEY AUTO_INCREMENT,
                         name varchar(255),
                         address varchar(255),
                         internshipSupervisor varchar(255)
);

CREATE TABLE Mark (
                      id int PRIMARY KEY AUTO_INCREMENT,
                      techMark double,
                      comMark double
);

CREATE TABLE Deliverable (
                             id int PRIMARY KEY AUTO_INCREMENT,
                             cdc boolean,
                             evaluationSheet boolean,
                             webSurvey boolean,
                             reportSent boolean,
                             defense boolean
);

CREATE TABLE Visit (
                       id int PRIMARY KEY AUTO_INCREMENT,
                       visitSheet boolean,
                       visitPlanned boolean,
                       visitDate date,
                       visitDone boolean
);

ALTER TABLE Internship ADD FOREIGN KEY (idTutor) REFERENCES Tutor (id);

ALTER TABLE Internship ADD FOREIGN KEY (idStudent) REFERENCES Student (id);

ALTER TABLE Internship ADD FOREIGN KEY (idCompany) REFERENCES Company (id);

ALTER TABLE Internship ADD FOREIGN KEY (idMark) REFERENCES Mark (id);

ALTER TABLE Internship ADD FOREIGN KEY (idDeliverable) REFERENCES Deliverable (id);

ALTER TABLE Internship ADD FOREIGN KEY (idVisit) REFERENCES Visit (id);


-- Insert data into Tutor table
INSERT INTO Tutor (username, password, firstname, lastname)
VALUES
    ('johndoe', '$2a$10$xlf0zxAhJ3anqikOJhoO7OUqno7f.PIhUdYRqre7DIAkLQjQxNs0a', 'John', 'Doe'),
    ('janedoe', '$2a$10$kOtwKEbhH6sgl4uhCCx/mOLzoac2gqcalCReTvV1EL8jr8ogzt2ES', 'Jane', 'Doe'),
    ('bobsmith', '$2a$10$phy/3ziEkOP.ON4HtNrWw.51oOwNPE845mrwoIkBJw7c3qHYRhETO', 'Bob', 'Smith');

-- Insert data into Student table
INSERT INTO Student (firstname, lastname, classGroup)
VALUES
    ('Alice', 'Jones', 'Class A'),
    ('Ben', 'Lee', 'Class A'),
    ('Charlie', 'Kim', 'Class B'),
    ('Diana', 'Nguyen', 'Class B');

-- Insert data into Company table
INSERT INTO Company (name, address, internshipSupervisor)
VALUES
    ('ABC Company', '123 Main St', 'John Smith'),
    ('XYZ Corporation', '456 1st Ave', 'Jane Williams'),
    ('123 Industries', '789 Elm St', 'Bob Johnson');

-- Insert data into Mark table
INSERT INTO Mark (techMark, comMark)
VALUES
    (85.0, 90.0),
    (80.0, 85.0),
    (90.0, 95.0);

-- Insert data into Deliverable table
INSERT INTO Deliverable (cdc, evaluationSheet, webSurvey, reportSent, defense)
VALUES
    (1, 0, 1, 1, 0),
    (0, 1, 0, 1, 1),
    (1, 1, 1, 0, 0);

-- Insert data into Visit table
INSERT INTO Visit (visitSheet, visitPlanned, visitDate, visitDone)
VALUES
    (1, 0, '2022-05-01', 0),
    (1, 1, '2022-06-15', 1),
    (0, 1, '2022-07-10', 0);

-- Insert data into Internship table
INSERT INTO Internship (idStudent, idCompany, idTutor, idMark, idVisit, idDeliverable, currentYear, startingDate, endingDate, missionDescription, commentary)
VALUES
    (1, 1, 1, 1, 1, 1, 2022, '2022-03-01', '2022-08-31', 'Software development', 'Excellent performance'),
    (2, 2, 2, 2, 2, 2, 2022, '2022-04-01', '2022-09-30', 'Database management', 'Good job'),
    (3, 3, 3, 3, 3, 3, 2022, '2022-05-01', '2022-10-31', 'Web design', 'Needs improvement');
